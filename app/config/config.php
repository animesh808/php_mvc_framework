<?php
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'YOUR_DBNAME');
    define('DB_USER', 'YOUR_DBUSER');
    define('DB_PASSWORD', 'YOUR_DBPASSWORD');

    define('APPROOT', dirname(dirname(__FILE__)));
    define('URLROOT', 'YOUR_APPURL');
    define('APPNAME', 'YOUR_APPNAME');
?>